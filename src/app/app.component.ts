import { Component, OnInit } from '@angular/core';
import { ProjectService } from './project.service';
import { Project } from './project';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'google-proj';
  private projectService: ProjectService;
  projects: Project[] = [];
  pageNumber: number = 1;
  constructor(projectService: ProjectService) { this.projectService = projectService; }
  ngOnInit() {
      this.updateProjects();
  }
  updateProjects() {
    this.projects = [];
    this.projectService.getProjectList(this.pageNumber).subscribe(
      (data: Project[]) => {
        data.forEach(proj => {
          this.projects.push(proj)
        })
      })
  }
  nextPage() {
    this.pageNumber += 1;
    this.updateProjects();
  }
  previousPage() {
    if (this.pageNumber > 1) {
      this.pageNumber -= 1;
      this.updateProjects();
    }
  }
}
