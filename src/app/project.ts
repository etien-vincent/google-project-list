export interface Project {
    name: string,
    description: string,
    forks_count: number,
    watchers_count: number,
    updated_at: string,
    language: string,
    license: { name: string }
}