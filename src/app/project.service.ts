import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { Project } from './project';
@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private http: HttpClient) { }
  private apiUrl = "https://api.github.com/orgs/google/repos";

  getProjectList(page: number): Observable<Project[]> {
    let params = new HttpParams()
      .set('org', "google")
      .set('sort', "updated")
      .set('per_page', 10)
      .set('page', page);
    return this.http.get<Project[]>(this.apiUrl, { params });
  }
}
